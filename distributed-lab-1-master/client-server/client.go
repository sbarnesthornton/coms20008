package main

import (
	"net"
)

func main() {
	msg := "hello"
	conn, _ := net.Dial("tcp", "127.0.0.1:8030")
	fmt.fprintf(conn, msg)
}
